# Programando ordenadores en los 80 y ahora. ¿Qué ha cambiado? (2016)
```plantuml
@startmindmap
*[#lightblue] Programando ordenadores \nen los 80 y ahora
	*[#Green] Sistemas antiguos
		*[#Orange] Es cualquier sistema que ya no esta a la venta o\n ya no tuvo una continuidad.
		*[#Orange] Ordenadores de los años 80´s y 90´s.
			*[#Orange] Programacion de esta época.
			*[#Orange] La potencia de las maquinas antiguas a diferencia\nde las actuales es muy grande.
			*[#Orange] Las maquinas de esa epoca no eran muy distintas\nen velocidades a las demas pero si que las hacian distintas.
			*[#Orange] Funcionan igual que el primer dia.
			*[#Orange] Tienen cuello de botella.
	*[#Green] Sistemas actuales
		*[#Orange] Es el que tenemos en casa o el que\npuedes comprar en una tienda.
		*[#Orange] Los ordenadores de ahora no funcionaran igual dentro de 1 mes o un cierto tiempo.
	*[#Green] Lenguajes
		*[#Orange] Bajo nivel.
			*[#Orange] Como no habia programadores que hicieran el trabajo,\nlo tenia que hacer el propio programador.
			*[#Orange] Se programaba en ensamblador.
				*[#Orange] Conocer la arquitectura.
				*[#Orange] Aprovechar bien los recursos.
				*[#Orange] Cuando el sistema fallaba tenias que checar \nmanualmente cual era el error.
			*[#Orange] Se tenia muchas falta de recursos.
			*[#Orange] Se pririzaba la eficiencia.
		*[#Orange] Alto nivel.
			*[#Orange] Programas que compilan e interpretan los datos.
				*[#Orange] C, C++.
				*[#Orange]  Java.					
					*[#Orange] No se compila directamente en la maquina, sino en una maquina virtual.
			*[#Orange] Te permite programar con ciertas abstracciones que ensamblador no se podian.	
	
	*[#Green] Leyes
		*[#Orange] Ley de Moore
			*[#Orange] Cada 18 meses se puede generar una CPU el doble de rapido.
		*[#Orange] Ley de Page
			*[#Orange] cada 18 meses el software se vuelve el doble de lento.
 
@endmindmap
```
# Hª de los algoritmos y de los lenguajes de programación (2010)
```plantuml
@startmindmap
*[#lightblue] Hª de los algoritmos y de los\nlenguajes de programación (2010)
	*[#Green] Algoritmo.
		*[#Orange] Es una lista bien definida, ordebada y finita\nde operaciones que permite hallar la solucion a un problema.
		*[#Orange] Es un proceso sistematico y mecanico que lo puede\nhacer una maquina para resolver un problema.
			*[#Orange] Ejemplo
				*[#Orange] Multiplicar
				*[#Orange] Receta de cocina
			*[#Orange] Historia
				*[#Orange] Se emplearon calculos algoritmicos para describir calculos en la antigua mesopotamia	
				*[#Orange] En el siglo XVII Aparecieron las primeras ayudas mecanicas\npara el calculo en forma de calculadoras de sobremesa.
				*[#Orange] En el siglo XIX Se conciben las primeras maquinas programables.
		*[#Orange] Importancia.
			*[#Orange] Radica en la manera de llevar a cabo un proceso y resolver mecanicamente procesos matematicos.
		*[#Orange] Los algoritmos reciben una entrada y la transforman en una salida.
		*[#Orange] Costes
			*[#Orange] Razonables
				*[#Orange] Son algoritmos cuyo tiempo de ejecución crece despacio a medida \nque los problemas se van haciendo más grandes.
					*[#Orange] Ejemplo: Multiplicar dos números de muchas cifras, \estos se pueden considerar problemas razonables.
			*[#Orange] No Razonables
				*[#Orange] Se le llama exponenciales o super polinomiales a aquellos que se comportan muy mal.
					*[#Orange] Ejemplo: Añadir un dato más al problema hace que se duplique el tiempo
	*[#Green] Lenguajes de Programacion.
		*[#Orange] Instrumentos adecuados para comunicar los\nalgoritmos a las maquinas que han de ejecutarlos.
			*[#Orange] Paradigmas o familias de lenguajes.
				*[#Orange] FORTRAN años 1959.
					*[#Orange] Primer lenguaje de alto nivel de la historia.
					*[#Orange] Representante del paradigma tambien conocido como imperativo.
				*[#Orange] Surge LISP 1960
					*[#Orange] Este es un paradigma Funcional.
				*[#Orange] En 1968 aparece SIMULA
					*[#Orange] Lenguaje Orientado a Objetos.
				*[#Orange] en 1971 aparece PROLOG
					*[#Orange] Lenguaje que computa a base de formulas logicas.
				*[#Orange] Aparecen los programas concurrente y\nProgramas Paralelos.
					*[#Orange] Contiene muchas familias de lenguaje y\ndentro de cada una hay muchos lenguajes.
			*[#Orange] Lenguajes de Programacion actuales
				*[#Orange] Java con Paradigma Orientado a Objetos.
				*[#Orange] c++ con Paradigma Orientado a Objetos.
				*[#Orange] c con Paradigma Orientado a Objetos.
			*[#Orange] Lenguajes Lógicos
				*[#Orange] Se usan en empresas con problemas de optimizacion.
					*[#Orange] Ejemplos: Para hacer horarios, transporte, tripulaciones de vuelo.
			*[#Orange] Lenguajes Funcionales
				*[#Orange] Tiene una capacidad amplia para crear\n procesos paralelos distribuidos.
	*[#Green] Maquinas.
		*[#Orange] El primer computador de la historia se llamo "Maquina Analitica".
			*[#Orange] Esta era mecanica
			*[#Orange] Los programas estaban en unas tablas de maderas perforadas.
			*[#Orange] Lo mas novedoso de la maquina de Babbage era la memoria.
				*[#Orange] Aqui se dejan los resultados intermedios de los calculos.
	*[#Green] Informatica.
		*[#Orange] El manejo de procesadores de texto o hojas de calculo
		*[#Orange] Aportaciones
			*[#Orange] Máquinas muy rapidas
			*[#Orange] Notaciones precisas
	*[#Green] Dispositivos
		*[#Orange] Unidad de Babbage hacía las 4 operaciones basicas.
			*[#Orange] Sumar.
			*[#Orange] Resta
			*[#Orange] Multiplicacion
			*[#Orange] Divicion
	*[#Green] Límites Teoricos
		*[#Orange] En los años 30´s se demostro que hay problemas definidos\npero que aun no admiten algoritmos.
			*[#Orange] Problemas Indecidibles
				*[#Orange] Saber si el programa terminara o no.
				*[#Orange] Saber cuanta memoria va a consumir.
				*[#Orange] Cuanto tiempo tardara en ejecutarse.
	*[#Green] Clases de complejidad
		*[#Orange] P
			*[#Orange] Conjunto de los problemas que se pueden resolver en tiempo polimonial.
		*[#Orange] NP
			*[#Orange] Conjunto de problemas cuya solucion se puede probar que es buena en tiempo poliminial.	

		



@endmindmap
```
# Tendencias actuales en los lenguajes de Programación y Sistemas Informáticos. La evolución de los lenguajes y paradigmas de programación (2005)
```plantuml
@startmindmap
*[#lightblue] Tendencias actuales en los lenguajes de\nProgramación y Sistemas Informáticos
	*[#Green] Paradigmas
		*[#Orange] Es una forma de aproximarse a la solucion de un problema.
	*[#Green] Programacion Estructurada
		*[#Orange] No tenemos que pensar en las estructuras de la memoria.
			*[#Orange] Abstractas.
			*[#Orange] De control
		*[#Orange] El diseño del software es mas modular.
		*[#Orange] Ejemplos:
			*[#Orange] Basic
			*[#Orange] C
			*[#Orange] Pascal
			*[#Orange] Fortran
		*[#Orange] Java con programacion orientado a objetos
			*[#Orange] Las nesecidades del cliente\npueden cambiar
			*[#Orange] Algoritmos de diálogo \nentre los objetos
	*[#Green] Paradigma Funcional
		*[#Orange] Este se apoya mucho en las matematicas.
		*[#Orange] Se basa en la recursividad.
		*[#Orange] Lenguajes
			*[#Orange] ML
			*[#Orange] HOPE
			*[#Orange] Haskel
	*[#Green] Paradigma Logico
		*[#Orange] Se basa en expresiones logicas
		*[#Orange] Usa predicados logicos
		*[#Orange] Se usa la recursividad
		*[#Orange] No vamos a tener ni numeros, ni letras, ni operaciones aritmeticas
		*[#Orange] Lenguajes
			*[#Orange] Prolog
			*[#Orange] Mercury
			*[#Orange] CLP
	*[#Green] Programacion Concurrente
		*[#Orange] Es una manera de concebir los problemas que surgen en los sistemas informaticos y \nque pretende dar solución a multitud de usuarios que accedan simultáneamente
		*[#Orange] Se deben programar ciertas politicas de sincronización y coordinación
		*[#Orange] Ejemplos:	
			*[#Orange] Una cuenta vacía no puede extraer dinero 
			*[#Orange] Una cuenta que esta haciendo una transacción no \npuede realizar otra simultáneamente  
	*[#Green] Programacion Distribuida
		*[#Orange] Surge desde que los ordenadores se pueden comunicar entre si.
		*[#Orange] Ejemplo:
			*[#Orange] Proyectp SETI@home
	*[#Green] Programacion basada en Componentes
		*[#Orange] Se puede ver como un componente es un conjunto\nde objetos que nos da una funcionalidad mayor
		*[#Orange] Permite la reutilización a la \nhora crear o de construir nuevos programas
	*[#Green] Programacion Orientada a Aspectos
		*[#Orange] Implementar el esqueleto del programa, incorporamos \nnuevos aspectos hasta construir nuestro sistema. 
			*[#Orange] Se agrega capas en base a la necesidad del programa
		*[#Orange] Todos los aspectos son separados
	*[#Green] Programación orientada a agentes de\nsoftware y los sistemas multiagentes
		*[#Orange] Son aplicaciones informáticas con capacidad \nde decidir cómo deben actuar para alcanzar sus objetivos
		*[#Orange] Los Agentes son entidades autónomas que pueden\npercibir su entorno y\nTienen la necesidad de alcanzar sus objetivos
		*[#Orange] Sistemas Multiagente
			*[#Orange] Habilidad social para interacion como:\ndialogo, cooperación, coordinación y negociación	



@endmindmap
